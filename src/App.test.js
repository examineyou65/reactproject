import React from 'react';
import renderer from 'react-test-renderer';
import { mount, shallow } from 'enzyme';
import App from './App';
import Header from './component/organisms/header';
import { configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import configureStore from 'redux-mock-store';
import { Provider } from 'react-redux';
import { BrowserRouter, MemoryRouter } from 'react-router-dom';
import { LoginPage } from './component/pages/Login';


const mockStore = configureStore([]);
//configure({ adapter: new Adapter() });
describe('App Test', () => {
  let store;
  let component;
  beforeEach(() => {
    store = mockStore({
      userInfo: { user: 'test' },
    });
    component = renderer.create(
      <Provider store={store}>
        <BrowserRouter>
          <App />
        </BrowserRouter>
      </Provider>
    );
  });

  it('renders header', () => {
    const wrapper = shallow(<App />);
    expect(wrapper.find("Connect(Header)")).toHaveLength(1)
    //expect(component.toJSON()).toMatchSnapshot();
  });
  it('Header component with Login', () => {
    const wrapper = mount(
      <Provider store={store}>
        <MemoryRouter initialEntries={['/']}>
          <App />
        </MemoryRouter>
      </Provider>
    );
    expect(wrapper.find(LoginPage).find("ul")).toHaveLength(0);
  });
});