import { mergeMap, filter, catchError } from 'rxjs/operators';
import { APPCONFiG } from '../config';
import axios from 'axios';

export const SAVE_LOGGED_USER = "SAVE_LOGGED_USER";
export const GET_LOGGED_USER = "GET_LOGGED_USER";
export const REGISTER_NEW_USER = "REGISTER_NEW_USER";
export const REGISTER_SUCCESS = "REGISTER_SUCCESS";
export const REGISTER_FAILURE = "REGISTER_FAILURE";
export const VALIDATE_USER = "VALIDATE_USER";
export const VALIDATE_SUCCESS = "VALIDATE_SUCCESS";
export const VALIDATE_FAILURE = "VALIDATE_FAILURE";

const initState = {
    userInfo: {},
    isValidUser: false
}
export const UserReducer = (state = initState, action) => {
    switch (action.type) {
        case SAVE_LOGGED_USER:
            return Object.assign({}, state, { userInfo: action.userDetail });
        case VALIDATE_SUCCESS:
            return Object.assign({}, state, { isValidUser: true })
        case VALIDATE_FAILURE:
            return Object.assign({}, state, { isValidUser: false })
        default:
            return state;
    }
}


export const RegisterUserepic = action$ => action$.pipe(
    filter(action => action.type === REGISTER_NEW_USER),
    mergeMap(async (action) => {
        let actionData = action.userDetail;
        const url = APPCONFiG + "/validateUser";
        const price = await axios({
            method: 'post',
            url: url,
            data: JSON.stringify(actionData)
        }).then(res => res.data);;
        return Object.assign({}, action, { type: REGISTER_SUCCESS, price });
    }),
    catchError(err => Promise.resolve({ type: REGISTER_FAILURE, message: err.message }))
);

export const validateUserEpic = action$ => action$.pipe(
    filter(action => action.type === VALIDATE_USER),
    mergeMap(async (action) => {
        let actionData = {
            userID: action.userName
        }
        const url = APPCONFiG.serverPath + "validateUser";
        const response = await axios({
            method: 'post',
            url: url,
            data: JSON.stringify(actionData)
        });
        console.log(response);
        if (response != null) {
            return Object.assign({}, action, { type: VALIDATE_SUCCESS, response });
        }
    }),
    catchError(err => Promise.resolve({ type: VALIDATE_FAILURE, message: err.message }))
);
