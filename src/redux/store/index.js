import { createStore, applyMiddleware, compose } from "redux";
import { rootReducer } from "../reducer";
import { composeWithDevTools } from "redux-devtools-extension";
import { createEpicMiddleware } from 'redux-observable';
import { combineEpics } from 'redux-observable';
import { RegisterUserepic, validateUserEpic } from '../userModule'
import { from } from "rxjs";
const rootEpic = combineEpics(
    RegisterUserepic,
    validateUserEpic
);
const epicMiddleware = createEpicMiddleware();
const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const middleware = [
    epicMiddleware
]
const Store = createStore(rootReducer, composeEnhancer(applyMiddleware(...middleware)));
epicMiddleware.run(rootEpic);
export default Store;