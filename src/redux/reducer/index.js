import { combineReducers } from 'redux';
import {  UserReducer } from '../userModule';
import { reducer as formReducer } from 'redux-form';
export const rootReducer = combineReducers({
    userInfo: UserReducer,
    form: formReducer
});