import React from 'react';
import './App.css';
import Header from './component/organisms/header';
import { Switch, Route } from 'react-router-dom';
import { LoginPage } from './component/pages/Login';
import SignupForm from './component/organisms/signupForm';
import { APPCONFiG } from './config';
function App() {
  return (
    <div className='content-wrapper'>
      <p>{APPCONFiG.serverPath}</p>
      <Header></Header>
      <div className='content-sec'>
        <Switch>
          <Route exact path='/' component={LoginPage} />
          <Route exact path='/signup' component={SignupForm} />
        </Switch>
      </div>
    </div >
  );
}

export default App;
