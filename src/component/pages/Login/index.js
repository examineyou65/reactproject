import React from 'react';
import LoginForm from "../../organisms/loginForm"
export const LoginPage = () => {
    return <div className='login_container'>
        <LoginForm />
    </div>
}