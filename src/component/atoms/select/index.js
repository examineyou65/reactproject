import React from 'react'
import styled from "styled-components";

export const Select = styled.select`
width: 100%;
    padding: 10px 5px;
    margin-bottom: 10px;
`

const Option = styled.option`
width:100%;
`
export const renderSelectField = ({ input, label, type, meta: { touched, error }, children }) => (
    <div>
        <label>{label}</label>
        <div>
            <Select {...input}>
                {children}
            </Select>
            {touched && error && <span>{error}</span>}
        </div>
    </div>
)

export const DropDownOptions = (props) => {
    let { optionList, isSelect, SelectText, id } = props;
    return <>{
        isSelect ? <Option value={SelectText}>{SelectText}</Option> : null
    }{
            optionList.map((item, index) => {
                return <Option key={id + "_" + index} value={item.key}>{item.displayText}</Option>
            })
        }
    </>

}