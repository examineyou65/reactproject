import React from 'react';
import styled from 'styled-components';

export const Input = styled.input`
width: 100%;
padding: 10px 5px;
border-radius: 5px;
border: 1px solid gray;
`
export const renderField = ({
    input,
    label,
    type,
    meta: { touched, error, warning }
}) => (
        <div className='input_field'>
            <label>{label}</label>
            <div>
                <Input {...input} placeholder={label} type={type} />
                {touched &&
                    ((error && <span>{error}</span>) ||
                        (warning && <span>{warning}</span>))}
            </div>
        </div>
    )