import React from "react";
import { mount, shallow } from "enzyme";
import configureStore from 'redux-mock-store';
import { Provider } from "react-redux";
import { BrowserRouter, Link } from "react-router-dom";
import loginForm from "./loginForm";
const mockStore = configureStore([]);

describe("Login Form Test Component", () => {
    let store;
    let emptyStore;
    beforeEach(() => {
        store = mockStore({
            userInfo: { user: 'test' },
        });
        emptyStore = mockStore({
            userInfo: null,
        })
    });
    test("login form test component", () => {
        let wrapper = mount(<Provider store={store}>
            <BrowserRouter>
                <loginForm></loginForm>
            </BrowserRouter>
        </Provider>
        );
        expect(wrapper.find("ul")).toHaveLength(1)
    }
    )
    test("Header component render without menu", () => {
        let wrapper = mount(<Provider store={emptyStore}>
            <BrowserRouter>
                <loginForm></loginForm>
            </BrowserRouter>
        </Provider>
        );
        expect(wrapper.find("ul")).toHaveLength(0)
    })
   

})