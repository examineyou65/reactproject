import React from 'react';
import { connect } from 'react-redux'
import { Link } from 'react-router-dom';
const Header = (props) => {
    return <div className='cover-content'>
        <div className='menu-container'>
            {
                props.userInfo!=null?
                <ul>
                    <Link to='home'>Home</Link>
                    <Link to='addPlayer'>Add Player</Link>
                    <Link to='playerPeformance'>Player Performance</Link>
                    <Link to='winpercentage'>Win/Lost Percentage</Link>
            </ul>:null
            }
        </div>
    </div>
}

const mapStatetoProps = (state) => {
    return {
        userInfo: state.userInfo
    }

}


export default connect(mapStatetoProps, null)(Header);