import React from "react";
import { mount, shallow } from "enzyme";
import Header from "./header";
import configureStore from 'redux-mock-store';
import { Provider } from "react-redux";
import { BrowserRouter, Link } from "react-router-dom";
const mockStore = configureStore([]);

describe("Header Test Component", () => {
    let store;
    let emptyStore;
    beforeEach(() => {
        store = mockStore({
            userInfo: { user: 'test' },
        });
        emptyStore = mockStore({
            userInfo: null,
        })
    });
    test("Header component render with menu", () => {
        let wrapper = mount(<Provider store={store}>
            <BrowserRouter>
                <Header ></Header>
            </BrowserRouter>
        </Provider>
        );
        expect(wrapper.find("ul")).toHaveLength(1)
    }
    )
    test("Header component render without menu", () => {
        let wrapper = mount(<Provider store={emptyStore}>
            <BrowserRouter>
                <Header ></Header>
            </BrowserRouter>
        </Provider>
        );
        expect(wrapper.find("ul")).toHaveLength(0)
    })
    test("Header component render with Link", () => {
        let wrapper = mount(<Provider store={store}>
            <BrowserRouter>
                <Header ></Header>
            </BrowserRouter>
        </Provider>
        );
        expect(wrapper.find(Link)).toHaveLength(4)
    })
    test("Header component render with Link", () => {
        let wrapper = mount(<Provider store={store}>
            <BrowserRouter>
                <Header ></Header>
            </BrowserRouter>
        </Provider>
        );
        expect(wrapper.find(Link).first().props().to).toBe('home');
    })

})