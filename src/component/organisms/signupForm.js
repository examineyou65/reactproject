import React from 'react'
import { Field, reduxForm } from 'redux-form'
import { renderField } from '../atoms/input'
import { Button } from '../atoms/button'
import { SAVE_LOGGED_USER, VALIDATE_USER } from '../../redux/userModule'
import { Select, DropDownOptions, renderSelectField } from '../atoms/select'

const required = value => (value || typeof value === 'number' ? undefined : 'Required')
const requiredSelect = value => (value || typeof value === 'number' ? undefined : 'Select Required Required')
const email = value =>
    value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)
        ? 'Invalid email address'
        : undefined


const onRegisteruser = (userDetail, scope) => {
    scope.dispatch({ type: SAVE_LOGGED_USER, userDetail })
}
let options = [{
    key: "childhood",
    displayText: "What was your childhood nickname?"
}, {
    key: "favoriteFriend",
    displayText: "What is the name of your favorite childhood friend?"
}, {
    key: "sixthgrade",
    displayText: "What school did you attend for sixth grade?"
}, {
    key: "sibling",
    displayText: "In what city does your nearest sibling live?"
}]

const validatefunction = values => {
    console.log("%%%%%%%%%%%%%%%%%%%%%%%%%%%");
    const errors = {}
    if (values.secretques == "") {
        errors.secretques = 'Select something'
    }
    return errors
}

const validateUser = (event, scope) => {
    var userName = event.target.value;
    scope.dispatch({ type: VALIDATE_USER, userName })
}


const SignUpForm = props => {
    const { handleSubmit, pristine, reset, submitting } = props
    return (
        <form onSubmit={handleSubmit((values) => { onRegisteruser(values, props); })}>
            <Field
                name="username"
                type="text"
                component={renderField}
                label="Username"
                validate={[required, email]}
                onBlur={(event) => { validateUser(event, props) }}
            />
            <Field
                name="password"
                type="password"
                component={renderField}
                label="password"
                validate={required}
            />
            <Field name="secretques" validate={requiredSelect} component={renderSelectField} label="Secret Question">
                <DropDownOptions id="secrequse" optionList={options} isSelect={true} SelectText=""></DropDownOptions>
            </Field>
            <Field
                name="answer"
                type="text"
                component={renderField}
                label="Answer"
                validate={required}
            />
            <div>
                <Button type="submit" disabled={submitting}>
                    Submit
                </Button>

            </div>
        </form>
    )
}

export default reduxForm({
    form: 'signupForm', // a unique identifier for this form
    validatefunction
})(SignUpForm)