import React from 'react'
import { Field, reduxForm } from 'redux-form'
import { renderField } from '../atoms/input'
import { Button } from '../atoms/button'
import { SAVE_LOGGED_USER } from '../../redux/userModule'
import { withRouter } from "react-router";
const required = value => (value || typeof value === 'number' ? undefined : 'Required')
const maxLength = max => value =>
    value && value.length > max ? `Must be ${max} characters or less` : undefined
const maxLength15 = maxLength(15)
export const minLength = min => value =>
    value && value.length < min ? `Must be ${min} characters or more` : undefined
export const minLength2 = minLength(2)
const number = value =>
    value && isNaN(Number(value)) ? 'Must be a number' : undefined
const minValue = min => value =>
    value && value < min ? `Must be at least ${min}` : undefined
const minValue13 = minValue(13)
const email = value =>
    value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)
        ? 'Invalid email address'
        : undefined
const tooYoung = value =>
    value && value < 13
        ? 'You do not meet the minimum age requirement!'
        : undefined
const aol = value =>
    value && /.+@aol\.com/.test(value)
        ? 'Really? You still use AOL for your email?'
        : undefined
const alphaNumeric = value =>
    value && /[^a-zA-Z0-9 ]/i.test(value)
        ? 'Only alphanumeric characters'
        : undefined
export const phoneNumber = value =>
    value && !/^(0|[1-9][0-9]{9})$/i.test(value)
        ? 'Invalid phone number, must be 10 digits'
        : undefined

const onSubmitLogin = (userDetail, scope) => {
    scope.dispatch({ type: SAVE_LOGGED_USER, userDetail })
}

const routeToSignup = (scope) => {
    scope.history.push('signup')
}

const LoginForm = withRouter(props => {
    const { handleSubmit, pristine, reset, submitting } = props
    return (
        <form onSubmit={handleSubmit((values) => { onSubmitLogin(values, props); })}>
            <Field
                name="username"
                type="text"
                component={renderField}
                label="Username"
                validate={[required, email]}
            />
            <Field
                name="password"
                type="password"
                component={renderField}
                label="password"
                validate={required}
            />
            <div>
                <Button type="submit" disabled={submitting}>
                    Submit
                </Button>
                <Button type="button" onClick={() => { routeToSignup(props) }} >
                    SignUp
                </Button>

            </div>
        </form>
    )
})

export default reduxForm({
    form: 'loginForm' // a unique identifier for this form
})(LoginForm)